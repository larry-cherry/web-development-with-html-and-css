# Web Development with HTML and CSS

Introduction to web development concepts using HTML and CSS

- [Lesson 1: Introduction](introduction/introduction.md)
- [Lesson 2: HTML Structure](html_structure/html_structure.md)
- [Lesson 3: Introduction to CSS](introduction_to_css/introduction_to_css.md)
- [Lesson 4: HTML Attributes](html_attributes/html_attributes.md)
- [Lesson 5: Text Styling With Google Fonts](text_styling_with_google_fonts/text_styling_with_google_fonts.md)
- [Lesson 6: Containers](containers/containers.md)
- [Lesson 7: Classes](classes/classes.md)
- [Lesson 8: Box Model](box_model/box_model.md)
- [Lesson 9: Positioning](positioning/positioning.md)
- [Lesson 10: Pseudo Selectors](pseudo_selectors/pseudo_selectors.md)
- [Lesson 11: Final Project Part 1](final_project_part1/final_project_part1.md)
- [Lesson 12: Final Project Part 2](final_project_part2/final_project_part2.md)






