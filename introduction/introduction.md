
Lesson 1: Introduction

Objective: Become familar with what html is and the basic tools for the course.

**Required Materials:** You will need a computer that can use a text editor like [VSCode](https://code.visualstudio.com/), [Atom](https://atom.io/), or [Notepad++](https://notepad-plus-plus.org/). You will need a web browser preferably [Google Chrome](https://www.google.com/chrome/).

**Time:** 30 Minututes

Procedure:
- Each lesson will have between 1 to 5 exercises with the potentail for a bonus exercise. It is import to follow along and type everything shown. The homework section is ment to reinforce concepts. You will need to do the homework on your own but are welcome to use any documentation to help you with your homework. 

**Exercise 1:**
- test 1

**Exercise 2:**
- test 2

**Exercise 3:**
- test 3

**Exercise 4:**
- test 4

**Exercise 5:**
- test 5

**Homework 1:**
- Finish and type up essays, read acts 1 & 2 of A Midsummer Night’s Dream for next class. Start thinking about overall themes in Shakespeare to get ready for midterm paper.

**Homework 2:**
- n